import requests
import json
import time
import collections

dataPair = collections.namedtuple('dataPair', ['data', 'status_code'])
# datos disponibles
# invocador: runas, maestrias, id, nivel, icono
summonerDataByNames = 'http://{region}.api.pvp.net/api/lol/{region}/v1.4/summoner/by-name/{summonerNames}'
# partidas recientes
recentMatchesData = 'http://{region}.api.pvp.net/api/lol/{region}/v1.3/game/by-summoner/{summonerId}/recent'
#nombres de campeones y otros datos
championsData = 'http://{region}.api.pvp.net/api/lol/static-data/{region}/v1.2/champion'

debug = 1

#niveles de depuracion
#0: funcionamiento normal
#1: pide usuario y region y muestra informacion sobre las funciones. requestRecentMatches y extractData escriben un archivo con su devolucion
#2: no pide usario y region y muestra informacion sobre las funciones. requestRecentMatches y extractData escriben un archivo con su devolucion

developer_key = '30ede2e5-d44a-478c-8ce0-c7e2f6e90cc9'

parametros = {'api_key': developer_key}

def getAccountData():
    user_summoner = raw_input("Cuenta: ")
    user_region = raw_input("Region: ")
    return user_summoner, user_region

def httpErroHanlder(error_code):
    #Esta funcion tiene que encargarse de interpretar los errores que se pueden producir al hacer un pedido.
    #El problema es que cuando se produce un error puede darse el caso de que una funcion que tenia que devolver
    #un valor (particularmente en los errores 500, 503 y 401) ahora no puede devolver nada porque el servidor
    #(de Riot) no devolvio el valor esperado.
    #
    #
    #

    if error_code == 400:
        print('Bad Request')
    elif error_code == 401:
        print('Unauthorized')
    elif error_code == 404:
        print('Game data not found')
    elif error_code == 429:
        print('Rate limit exceeded')
    elif error_code == 500:
        print('Internal server error')
    elif error_code == 503:
        print('Service unavailable')


def requestId(nombreInvocador, region):  #devuelve el id numerico de un usuario
    #Las excepciones deben ser atrapadas en el lugar en el que puedan ocurrir (aca debajo de la proxima linea, por
    #ejemplo puede saltar un ConnectionError cuando no hay internet). Si por alguna razon esta funcion no puede
    #devolver el id del invocador va a devolver 'None' y main se va a encargar de actuar como sea necesario.

    try:
        pedido = requests.get(summonerDataByNames.format(region=region, summonerNames=nombreInvocador), params=parametros)
    except requests.ConnectionError:
        print("Error de conexion. Estas conectado a Internet?")
        return None
    except requests.HTTPError:
        print("Respusta invalida del servidor. Informar al desarrollador")
        return None
    except requests.Timeout:
        print("El servidor no respondio lo suficientemente rapido")
        return None

    if debug > 0:
        print('Estado de respuesta: ' + str(pedido.status_code))
    if pedido.status_code == 200:
        #datos = pedido.json()
        #return datos[nombreInvocador.lower()]['id']
        datos = dataPair(pedido.json()[nombreInvocador.lower()]['id'], pedido.status_code)
        return datos
    elif pedido.status_code == 400:
        print('Sintaxis erronea en el pedido. Informar al desarrollador')
        #devolver [jsonData, statusCode]
        return None
    elif pedido.status_code == 401:
        print('Clave no autorizada para realizar este pedido. Informar al desarrollador')
        #devolver [jsonData, statusCode]
        return  None
    elif pedido.status_code == 404:
        print('No existen datos para ese nombre de Invocador. Te equivocaste de nombre?')
        datos = dataPair(None, pedido.status_code)
        #devolver [jsonData, statusCode] y pedir datos de usuario otra vez
        return datos
    elif pedido.status_code == 429:
        print('Limite de pedidos excedidos')
        #devolver [jsonData, statusCode]
        return None
    elif pedido.status_code == 500:
        print('Error inesperado del servidor de Riot Games')
        #devolver [jsonData, statusCode]
        return None
    elif pedido.status_code == 503:
        print('Servicio temporalmente no disponible')
        #devolver [jsonData, statusCode]
        return None


def requestRecentMatches(invocadorId, region):  #obtiene datos sobre las partidas recientes;
    pedido = requests.get(recentMatchesData.format(region=region, summonerId=invocadorId), params=parametros)
    partidas = pedido.json()
    #print(partidas)
    if debug == 1 or debug == 2:
        new_file = open('requestRecentMatches.txt', 'w')
        new_file.write(json.dumps(partidas, sort_keys=True, indent=4, separators=(',', ': ')))
        new_file.close()

    return partidas


def getChampData(region):  #descarga los datos de los campeones
    #no se si voy a tener una base de datos local de los campeones o hacer un pedido cada vez que necesite un nombre
    pedido = requests.get(championsData.format(region=region), params=parametros)
    datos = pedido.json()
    if debug == 1 or debug == 2:
        new_file = open('champ_data.txt', 'w')
        new_file.write(json.dumps(datos, sort_keys=True, indent=4, separators=(',', ': ')))


def getChampName(champ_id):
    champ_db_file = open('champ_data.txt', 'r')
    champ_db = json.loads(champ_db_file.read())
    champ_db_file.close()

    for tuple in champ_db['data'].iteritems():  #que concha significa tuple
        champ_dict = dict(tuple[1])
        if champ_id == champ_dict['id']:
            return champ_dict['name']


def extractData(riotData):  #quita los datos irrelevantes y los datos de las partidas que no pertenecen a la cola normal
    newData = {'games': []}
    for partida in riotData['games']:
        if partida['gameMode'] == 'CLASSIC' and partida['gameType'] == 'MATCHED_GAME':
            newData['games'].append({'createDate': partida['createDate'], 'championId': partida['championId'],
                                     'win': partida['stats']['win']})
    newData.update({'summonerId': riotData['summonerId']})
    if debug == 1 or debug == 2:
        new_file = open('extractData.txt', 'w')
        new_file.write(json.dumps(newData, sort_keys=True, indent=4, separators=(
            ',', ': ')))  #imprime el numero de partidas recientes que concuerdan con el criterio (partida normal)
        new_file.close()
        print('new data; games: ' + str(len(newData['games'])))

    return newData


def getWins(campeon):
    return campeon['wins']


def checkForDB(db_name):
    resultado = True
    try:
        open(db_name, 'r')
    except IOError:
        resultado = False

    return resultado

def orderDB(champions_list):
    return sorted(champions_list, key=getWins,
                  reverse=True)  #reverse tiene que ser True porque si no ordena de menor a mayor (i.e: campeones con 0 partidas ganadas, campeones con 1, campeones con 2...)


def createDB(id, region, db_name):  #crea una base de datos local con los datos extraidos de las partidas recientes
    datos = extractData(requestRecentMatches(id, region))
    db_file = open(db_name, 'w')
    db_contents = {'champions': [], 'creationDate': int(time.time()),
                   'firstMatch': 0}  #fecha de creacion de la base de datos
    campeones = []
    for entrada in datos['games']:
        campeones.append(entrada['championId'])

    campeones = list(
        set(campeones))  #quita los campeones repetidos (los sets son similares a listas, pero con elementos unicos)
    for item in campeones:
        db_contents['champions'].append({'championId': item, 'wins': 0, 'loses': 0})

    for campeon in db_contents['champions']:
        for id in datos['games']:
            if campeon['championId'] == id['championId']:
                if id['win'] == True:
                    campeon['wins'] += 1
                else:
                    campeon['loses'] += 1
                if debug == True:
                    print(
                        'campeon id:', campeon['championId'], ' wins: ', campeon['wins'], ' loses: ', campeon['loses'])

    if (debug == 1 or debug == 2):
        print('Ordenando lista de campeones en createDB')
        db_contents['champions'] = orderDB(db_contents['champions'])

    db_contents.update({'lastMatch': datos['games'][0]['createDate']})  #ultima partida "analizada"
    db_contents.update({'firstMatch': datos['games'][0][
        'createDate']})  #primer partida "analizada" (el unico proposito de esto es tener un registro, no cambia nunca mas)

    db_file.write(json.dumps(db_contents, sort_keys=True, indent=4,
                             separators=(',', ': ')))  #escribe los datos a un archivo de texto

    db_file.close()


def updateDB(id, region, db_name):  #actualiza la base de datos local
    datos = extractData(requestRecentMatches(id, region))

    if len(datos['games']) > 0:
        db_file = open(db_name, 'r')

        db_contents = json.loads(db_file.read())
        db_file.close()
        champions_in_db = []
        for champion in db_contents['champions']:
            champions_in_db.append(champion['championId'])

        recent_champions = []

        for partida in datos['games']:
            recent_champions.append(partida['championId'])

        recent_champions = list(set(recent_champions))

        missing_champions = list(set(recent_champions) - set(champions_in_db))

        if debug == 1 or debug == 2:
            print('En la base de datos: ', (champions_in_db))
            print('En las partidas recientes: ', recent_champions)
            print('Diferencia: ', missing_champions)

        if len(missing_champions) > 0:
            if debug == 1 or debug == 2:
                print('Faltan campeones en la DB local, agregando')
            for id in missing_champions:
                db_contents['champions'].append({'championId': id, 'wins': 0, 'loses': 0})
        else:
            if debug == 1 or debug == 2:
                print('No falta ningun campeon en la DB local')

        for partida in datos['games']:
            if partida['createDate'] > db_contents['lastMatch']:
                for campeon in db_contents['champions']:
                    if campeon['championId'] == partida['championId']:
                        if partida['win'] == True:
                            campeon['wins'] += 1
                        else:
                            campeon['loses'] += 1

        if (debug == 1 or debug == 2):
            print('Ordenando lista de campeones en updateDB')
            db_contents['champions'] = orderDB(db_contents['champions'])

        db_contents.update({'lastMatch': datos['games'][0]['createDate']})

        db_file = open(db_name, 'w')
        db_file.write(json.dumps(db_contents, sort_keys=True, indent=4, separators=(',', ': ')))
        db_file.close()


def topFive(db_name):
    db_file = open(db_name, 'r')
    db_contents = json.loads(db_file.read())
    db_file.close()

    string = 'Hasta ahora ganaste {wins} partidas con {name} y perdiste {loses}.'

    if len(db_contents['champions']) >= 5:
        for i in range(5):
            print(string.format(wins=db_contents['champions'][i]['wins'],
                                name=getChampName(db_contents['champions'][i]['championId']),
                                loses=db_contents['champions'][i]['loses']))
    else:
        for i in range(len(db_contents['champions'])):
            print(string.format(wins=db_contents['champions'][i]['wins'],
                                name=getChampName(db_contents['champions'][i]['championId']),
                                loses=db_contents['champions'][i]['loses']))

            # def showTopFive(db_name):
            # topfive = []

            # db_file = open(db_name, 'r')
            # db_contents = json.loads(db.file.read())
            # db_file.close()

def descargarDatos():
    user_data = getAccountData()
    user_id = requestId(user_data[0], user_data[1])

    if user_id.status_code == 200:
        requestRecentMatches(user_id)

print('Normal Logger 0.1')
print("Los abandonos cuentan como derrotas")  #diferencia entre abandono y derrota: las derrotas dan PI

if debug == 0 or debug == 1:
    user_data = getAccountData()
    user_summoner = user_data[0]
    user_region = user_data[1]
else:
    user_summoner = 'Copper81'
    user_region = 'las'

user_id = requestId(user_summoner, user_region)
print(user_id)

#hacer un pedido y comprobar la respuesta

while user_id.status_code != 200:
  if user_id.status_code == 404:
    user_data = getAccountData()
    user_id = requestId(user_data[0], user_data[1])
  if user_id.status_code == 429:
    #wait a bit
    user_data = getAccountData()
    user_id = requestId(user_data[0], user_data[1])

if user_id.status_code == 200:
    if debug > 0:
        print("Se obtuvo el id el usuario satisfactoriamente.")
    db_name = str(user_id.data) + 'DB'
    db_exists = True
    if debug > 0:
        print('UserID: ' + str(user_id.data))
    datos = requestRecentMatches(user_id.data, user_region)
    extracted = extractData(datos)

    db_exists = checkForDB(db_name)

    if db_exists is False:
        createDB(user_id.data, user_region, db_name)
    else:
        updateDB(user_id.data, user_region, db_name)

    #getChampData(user_region)
    topFive(db_name)

    raw_input('Para salir presiona ENTER')
    #falta tomar la base de datos y ordenar (y mostrar) los campeones en orden de partidas ganadas
    # string = 'Hasta ahora ganaste {wins} partidas con {nombreCampeon}'
    # nameDict = {champId:champName}
    # db_contents = base de datos
elif user_id.status_code == 404:
    while user_id.status_code == 404:
        user_data = getAccountData()
        user_id = requestId(user_data[0], user_data[1])

    if debug > 0:
        print("Se obtuvo el id el usuario satisfactoriamente.")
    db_name = str(user_id.data) + 'DB'
    db_exists = True
    if debug > 0:
        print('UserID: ' + str(user_id.data))
    datos = requestRecentMatches(user_id.data, user_region)
    extracted = extractData(datos)
    try:
        archivo = open(db_name, 'r')
    except IOError:
        db_exists = False

    if db_exists is False:
        createDB(user_id.data, user_region, db_name)
    else:
        updateDB(user_id.data, user_region, db_name)

    #getChampData(user_region)
    topFive(db_name)

    raw_input('Para salir presiona ENTER')
